<?php

namespace App\Http\Controllers;

use App\TodoList;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class TodoListController extends Controller
{
  public function index()
  {
    $todoList = TodoList::all();
    $todoid = TodoList::orderBy('id')->get();

    return view('index', compact('todoList','todoid'));
  }

  public function add(Request $request)
  {
    $rules = [
      'title' => 'required|min:3|max:255',
    ];
    $message = [
      'title.max' => 'อย่าพิมพ์ข้อความเกิน 255 ตัว!!',
      'title.min' => 'พิมพ์ข้อความให้เกิน 3 ตัว!!'
    ];
    $validator = Validator::make($request->all(), $rules, $message);
    if($validator->fails()){  
      return redirect()->back()->withErrors($validator)->withInput();
    }

    TodoList::create($request->all());
    return redirect()->back()->with('message', 'สร้างรายการเรียบร้อย');
  }

  public function complete(TodoList $TodoList)
  {
    $TodoList->update(['complete' => true]);
    return redirect()->back()->with('message', 'ทำรายการเสร็จสิ้น');

  }
   public function notcomplete(TodoList $TodoList)
  {
    $TodoList->update(['complete' => false]);
    return redirect()->back()->with('message', 'เปลี่ยนเป็นรายการยังไม่เสร็จสิ้น');

  }
  public function update(Request $request ,TodoList $TodoList)
  {
    $request->validate([
      'updatetitle' => 'required|min:3|max:255'
    ]);


    $TodoList->update(['title' => $request->updatetitle]);
    return redirect()->back()->with('message', 'เปลี่ยนรายการเสร็จสิ้น');

  }
    public function delete(TodoList $TodoList)
  {
    $TodoList->delete();
    return redirect()->back()->with('message', 'ลบรายการเสร็จสิ้น');

  }
}
