@extends('todoList.main')

@section('content')
   
    <h1 class="text-primary text-center w-100 py-5"> TO DO LIST</h1>
    <x-alert />
    <div class="card">
        <div class="card-header">
        <form method="post" action="/insert"  >  
            @csrf
            <div class="input-group">
                <input type="text" class="form-control" name="title" placeholder="Add Text">
                <button type="submit" class="btn btn-success rounded-right none-rounded-left">INSERT</button>
            </div>        
        </form> 
        </div>
        <ul class="list-group list-group-flush" style="transition:ease 0.2s;">
            @foreach($todoList as $todo)
            <li class="list-group-item d-flex justify-content-between " id="{{'list-'.$todo->id}}">
               
                <div class="d-flex justify-content-between align-items-center">
                    @if(!$todo->complete)
                    <div class="circle-green {{ !$todo->complete ? '' : 'bg-green' }}" 
                        onclick="event.preventDefault();
                        document.getElementById('form-complete-{{$todo->id}}').submit()"
                    >
                    </div>
                    <form action="{{route('complete',$todo->id)}}" id="{{'form-complete-'.$todo->id}}" method="post" class="d-none">
                        @csrf
                        @method('put')
                    </form>
                    @else  
                    <div class="circle-bg-green {{ $todo->complete ? '' : 'out-bg-green' }}" 
                        onclick="event.preventDefault();
                        document.getElementById('form-complete-not-{{$todo->id}}').submit()"
                    >
                    </div>
                    <form action="{{route('notcomplete',$todo->id)}}" id="{{'form-complete-not-'.$todo->id}}" method="post" class="d-none">
                        @csrf
                        @method('put')
                    </form>
                    @endif

                    @if(!$todo->complete)
                    <div class="list-todo">
                        <form action="{{route('update',$todo->id)}}" method="post" id="{{'form-update-'.$todo->id}}">
                            @csrf
                            @method('put')
                            <input type="text" class="form-control mr-3" name="updatetitle" id="{{'update-'.$todo->id}}" value="{{ $todo->title }}" disabled>   
                        </form>
                    </div>
                    @else
                    <div class="list-todo text-complete">
                        <form action="{{route('update',$todo->id)}}" method="post" id="{{'form-update-'.$todo->id}}">
                            @csrf
                            @method('put')
                            <input type="text" class="form-control mr-3" name="updatetitle" id="{{'update-'.$todo->id}}" value="{{ $todo->title }}" disabled>   
                        </form>
                    </div>
                    @endif
                </div>
                <div class=" d-flex justify-content-between align-items-center">
                    <button class="btn btn-warning mr-3 ml-3" id="{{'btn-update-'.$todo->id}}"
                        onclick="event.preventDefault();
                        if(document.getElementById('update-{{$todo->id}}').hasAttribute('disabled')){
                            document.getElementById('update-{{$todo->id}}').disabled = false;
                            document.getElementById('btn-update-{{$todo->id}}').innerHTML='Update';
                        }
                        else
                        {
                           document.getElementById('form-update-{{$todo->id}}').submit();
                           document.getElementById('btn-update-{{$todo->id}}').innerHTML='Edit';
                           document.getElementById('update-{{$todo->id}}').disabled = true;
                        }"
                    > Edit </button>
                    <button class="btn btn-danger"
                        onclick="event.preventDefault();
                        if(confirm('คุณจะลบรายการนี้ไหม?')){
                        document.getElementById('list-{{$todo->id}}').classList.add('animate__animated','animate__bounceOutRight');
                        document.getElementById('form-delete-{{$todo->id}}').submit();
                        }"
                    > Delete </button>
                    <form action="{{route('delete',$todo->id)}}" id="{{'form-delete-'.$todo->id}}" method="post" class="d-none">
                        @csrf
                        @method('delete')
                    </form>
                </div>
            </li>
            @endforeach
        </ul>
    </div>

@endsection