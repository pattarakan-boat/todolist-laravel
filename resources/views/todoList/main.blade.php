<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>To Do List</title>
        <!-- bootstrap -->
        <link rel="stylesheet" type="text/css" href="{{asset('css/app.css')}}">
        <!-- animate -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css"/>
    </head>
    <body>
        <div class="container">
            <div class="row justify-content-center align-items-center flex-column">
                @yield('content')
            </div>    
        </div>
        <!-- js bootstrap -->
       <script type="text/javascript" src="{{asset('js/app.js')}}"></script>
    </body>
</html>
