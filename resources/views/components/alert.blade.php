<div>
   @if(session()->has('message'))
   {{$slot}} 
      <div class="alert alert-success animate__animated animate__heartBeat">
         {{session()->get('message')}}
      </div>
   @elseif(session()->has('error'))
   {{$slot}}
      <div class="alert alert-danger animate__animated animate__heartBeat">
         {{session()->get('error')}}
      </div>
   @endif
   
   @if ($errors->any()) 
      <div class="alert alert-danger animate__animated animate__heartBeat">
         @foreach ($errors->all() as $error)
            {{ $error }}
         @endforeach
      </div>
   @endif
</div>