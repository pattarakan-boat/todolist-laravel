<?php

Route::get('/', 'TodoListController@index');
Route::post('/insert', 'TodoListController@add');
Route::put('/{TodoList}/complete', 'TodoListController@complete')->name('complete');
Route::put('/{TodoList}/notcomplete', 'TodoListController@notcomplete')->name('notcomplete');
Route::put('/{TodoList}/update', 'TodoListController@update')->name('update');
Route::delete('/{TodoList}/delete', 'TodoListController@delete')->name('delete');



